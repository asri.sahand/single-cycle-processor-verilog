module muxALUflag(input ALUflag,input[15:0] ALUout,readData,output[15:0] out);
  assign out=ALUflag?ALUout:readData;
endmodule