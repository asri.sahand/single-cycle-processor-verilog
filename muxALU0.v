module muxALU0(input[15:0] imd,Ri,input[1:0] ALU0,output[15:0] out);
  assign out=(ALU0==2'b00)?Ri:(ALU0==2'b01)?imd:(ALU0==2'b10)?16'b1:16'bz;
endmodule