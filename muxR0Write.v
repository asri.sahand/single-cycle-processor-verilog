module muxRoWrite(input R0Write,input[2:0] R0,output[2:0] out);
  assign out=R0Write?3'b0:R0;
endmodule