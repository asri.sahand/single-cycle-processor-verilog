library verilog;
use verilog.vl_types.all;
entity muxALUflag is
    port(
        ALUflag         : in     vl_logic;
        ALUout          : in     vl_logic_vector(15 downto 0);
        readData        : in     vl_logic_vector(15 downto 0);
        \out\           : out    vl_logic_vector(15 downto 0)
    );
end muxALUflag;
