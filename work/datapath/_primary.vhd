library verilog;
use verilog.vl_types.all;
entity datapath is
    port(
        clk             : in     vl_logic;
        Br              : in     vl_logic;
        rst             : in     vl_logic;
        R0Write         : in     vl_logic;
        ALUflag         : in     vl_logic;
        writeMem        : in     vl_logic;
        readMem         : in     vl_logic;
        Wen             : in     vl_logic;
        pcSel           : in     vl_logic_vector(1 downto 0);
        ALU0            : in     vl_logic_vector(1 downto 0);
        ALU1            : in     vl_logic_vector(1 downto 0);
        ALUop           : in     vl_logic_vector(8 downto 0);
        zeroFlag        : out    vl_logic;
        inss            : out    vl_logic_vector(15 downto 0)
    );
end datapath;
