library verilog;
use verilog.vl_types.all;
entity regFile is
    port(
        Wen             : in     vl_logic;
        clk             : in     vl_logic;
        regRead         : in     vl_logic_vector(2 downto 0);
        regWrite        : in     vl_logic_vector(2 downto 0);
        writeData       : in     vl_logic_vector(15 downto 0);
        readData        : out    vl_logic_vector(15 downto 0);
        readR0Data      : out    vl_logic_vector(15 downto 0)
    );
end regFile;
