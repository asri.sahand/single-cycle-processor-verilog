library verilog;
use verilog.vl_types.all;
entity muxRoWrite is
    port(
        R0Write         : in     vl_logic;
        R0              : in     vl_logic_vector(2 downto 0);
        \out\           : out    vl_logic_vector(2 downto 0)
    );
end muxRoWrite;
