library verilog;
use verilog.vl_types.all;
entity mem is
    port(
        adr             : in     vl_logic_vector(11 downto 0);
        writeData       : in     vl_logic_vector(15 downto 0);
        clk             : in     vl_logic;
        write           : in     vl_logic;
        read            : in     vl_logic;
        \out\           : out    vl_logic_vector(15 downto 0)
    );
end mem;
