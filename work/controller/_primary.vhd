library verilog;
use verilog.vl_types.all;
entity controller is
    port(
        ins             : in     vl_logic_vector(15 downto 0);
        zeroFlag        : in     vl_logic;
        clk             : in     vl_logic;
        Br              : out    vl_logic;
        R0Write         : out    vl_logic;
        ALUflag         : out    vl_logic;
        writeMem        : out    vl_logic;
        readMem         : out    vl_logic;
        Wen             : out    vl_logic;
        pcSel           : out    vl_logic_vector(1 downto 0);
        ALU0            : out    vl_logic_vector(1 downto 0);
        ALU1            : out    vl_logic_vector(1 downto 0);
        ALUop           : out    vl_logic_vector(8 downto 0)
    );
end controller;
