library verilog;
use verilog.vl_types.all;
entity muxALU1 is
    port(
        R0              : in     vl_logic_vector(15 downto 0);
        ALU1            : in     vl_logic_vector(1 downto 0);
        \out\           : out    vl_logic_vector(15 downto 0)
    );
end muxALU1;
