library verilog;
use verilog.vl_types.all;
entity muxALU0 is
    port(
        imd             : in     vl_logic_vector(15 downto 0);
        Ri              : in     vl_logic_vector(15 downto 0);
        ALU0            : in     vl_logic_vector(1 downto 0);
        \out\           : out    vl_logic_vector(15 downto 0)
    );
end muxALU0;
