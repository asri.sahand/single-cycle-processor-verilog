library verilog;
use verilog.vl_types.all;
entity insMem is
    port(
        adr             : in     vl_logic_vector(11 downto 0);
        clk             : in     vl_logic;
        ins             : out    vl_logic_vector(15 downto 0)
    );
end insMem;
