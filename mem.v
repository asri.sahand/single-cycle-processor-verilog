module mem(input[11:0] adr, input[15:0] writeData, input clk, write, read, output[15:0] out);
  reg[15:0] memory[0:2**12];
  initial $readmemb("data.txt", memory);
  assign out = (read)?memory[adr]: out;
  always@(posedge clk) begin
	 if(write) begin
	 	 memory[adr] = writeData;
	 end
  end
  always @(posedge clk) $display("memory[200] : %b", memory[200]);
endmodule


module memTB();
  reg[11:0] adr;
  reg[15:0] writeData;
  reg clk = 0, write = 0, read = 1;
  wire[15:0] out;
  
  mem mem1(adr, writeData, clk, write, read, out);
  
  initial repeat (20) #30 clk = ~clk;
  
  initial begin
    #100 adr = 10;
    #100 writeData = 20;
    #100 write = 1;
    #100 write = 0;
    #100 adr = 2;
  end
endmodule