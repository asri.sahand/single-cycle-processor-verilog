module mux12(input[11:0] a, b, c, input[1:0] pc_sel, output[11:0] out);
	assign out = (pc_sel == 2'b00)? a: (pc_sel == 2'b01)? b: (pc_sel == 2'b10)? c : 12'bz;
endmodule