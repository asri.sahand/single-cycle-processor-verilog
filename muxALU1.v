module muxALU1(input[15:0] R0,input[1:0] ALU1,output[15:0] out);
  assign out=(ALU1==2'b00)?R0:(ALU1==2'b01)?16'b0:(ALU1==2'b10)?16'b1:16'bz;
endmodule