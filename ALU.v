module ALU(input[15:0] a,b, input[8:0] func, output[15:0] out);
	wire[15:0] Clear, Moveto, Movefrom, Add, Sub, And, Or, Not;
	
	assign Clear = 0;
	assign Moveto = b;
	assign Movefrom = a;
	assign Add = a + b;
	assign Sub = b - a;
	assign And = a & b;
	assign Or = a | b;
	assign Not = ~a;
	
	assign out = func[0]? Clear : func[1]? Moveto : func[2]? Movefrom : func[3]? Add : func[4]? Sub : func[5]? And : func[6]? Or : func[7]? Not: out;

endmodule


module ALUTB();
  reg[15:0] a = 14, b = 27;
  reg[8:0] func;
  wire[15:0] out;
  
  ALU alu(a, b, func, out);
  
  initial begin
    #100 func = 9'b000000000;
    #100 func = 9'b000000001;
    #100 func = 9'b000000010;
    #100 func = 9'b000000100;
    #100 func = 9'b000001000;
    #100 func = 9'b000010000;
    #100 func = 9'b000100000;
    #100 func = 9'b001000000;
    #100 func = 9'b010000000;
    #100 func = 9'b100000001;
  end
  
endmodule