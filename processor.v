module processor();
  wire Br,R0Write,ALUflag,writeMem,readMem,Wen,zeroFlag;
  wire[1:0] pcSel,ALU0,ALU1;
  wire[8:0] ALUop;
  wire[15:0] ins;
  reg clk, rst;
  
  datapath dp(clk,Br,rst,R0Write,ALUflag,writeMem,readMem,Wen,pcSel,ALU0,ALU1,ALUop,zeroFlag,ins);
  controller c(ins,zeroFlag,clk,Br,R0Write,ALUflag,writeMem,readMem,Wen,pcSel,ALU0,ALU1,ALUop);
  
  initial repeat(100) #49 clk = ~clk;
  
  initial begin
  clk = 0;
  rst = 1;
  #50 rst = 0;
  end
  

endmodule
