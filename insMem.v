module insMem(input[11:0] adr, input clk,output[15:0] ins);
  reg[15:0] out;
  reg[15:0] memory[2**12:0];
  	initial $readmemb("instructions.txt",memory);
	 assign ins = memory[adr];
endmodule


module insMemTB();
  reg[11:0] adr;
  wire[15:0] ins;
  reg clk = 0;
  
  insMem insmem(adr, clk, ins);
  
  initial repeat (20) #30 clk = ~clk;
  
  initial begin
    #100 adr = 10;
    #100 adr = 2;
  end  
  
endmodule