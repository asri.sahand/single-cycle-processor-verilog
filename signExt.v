module signExt(input[11:0] in,output[15:0] out);
  assign out=in[11]?{4'b1111,in[11:0]}:{4'b0000,in[11:0]};
endmodule